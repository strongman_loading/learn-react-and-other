import React from 'react';
import globalStyles from '../../globalStyles/globalStyles.module.css';
import styles from './Section.module.css'
import Component from '../../components/component/Component'

export default class About extends React.Component<any, any> {

  render() {
    return (
      <section id="about" className={styles.mainDivAb}>
        <Component />
      </section>
    );
  }
}
