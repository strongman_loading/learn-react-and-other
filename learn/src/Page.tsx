import React from 'react';
import styles from './globalStyles/globalStyles.module.css';
import Section from './sections/Section/Section'

export interface IStatePage { }

export default class Page extends React.Component<any, IStatePage> {
  constructor(props: any) {
    super(props)
    this.state = {};
  }

  render() {
    return (
      <React.Fragment>
        <Section />
      </React.Fragment>
    );
  }
}